======
IZMENE
======

************
Uredi izmene
************

Kako dodamo novo izmeno?
++++++++++++++++++++++++

S klikom na gumb *Dodaj izmeno* se odpre obrazec, kjer najprej poljubno poimenujemo novo izmeno. Izmeni določimo
uro začetka in uro konca, ter na katere dneve v tednu poteka. Poljubno lahko določimo med katerima datuma je izmena
veljavna. Ta funkcija je prikladna za spreminjajoče se izmene, začasne izmene, turnuse in podobno. Če pustimo polje
datum *veljavnosti do* prazno, sistem obravnava izmeno kot veljavno za nedoločen čas.


.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/shifts_add_shift.png


Izmene so določene skozi zaposlitvene vloge (npr. Delavec na Stroju X, Vodja izmene, itd.), zato jih dodamo z
*Dodaj vlogo*. Uspešno dodane vloge se prikažejo v zavihku *Vloge*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/shift_add_role.png

S klikom na *Dodaj postavko* se odpre obrazec, kjer se določi *Urna postavka* za določeno *Vlogo* zaposlenega znotraj
dane *Izmene*. Hkrati lahko dodamo urno postavko za delo nadur ali na dela prostega dan (praznik). Prazniki so usklajeni
z uradnim koledarjem Republike Slovenije. S klikom na gumb *Potrdi* se postavka zabeleži v zavihku *Postavke*, ki
pregledno predstavlja postavke vseh vlog v vseh izmenah.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/shift_add_rate.png


Če želimo najti točno določeno izmeno, vlogo ali postavko v tabeli *Seznam izmen*, nam funkcija *Filtriraj tabelo* to
omogoča. Izberimo željen zavihek (Izmene / Vloge / Postavke) in po pritisku na gumb *Filtriraj tabelo* vpišemo iskalni
niz, ki nam vrne tabelo z iskanimi informacijami.

Kako uredim podatke o izmeni ali izmeno zbrišem?
++++++++++++++++++++++++++++++++++++++++++++++++

V podmeniju *Uredi izmene* se prikaže tabela *Seznam izmen* s podatki o imenu izmene, uri njenega začetka in konca,
(opcijsko) med katerimi datumi je uporabljena in stolpcem za urejanje |pencil| ali brisanje |delete| izmen.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/shift_edit.png

*******************
Razporedi zaposlene
*******************

V podmeniju *Razporedi zaposlene* dodelimo zaposlenim izmene na posamezni dan. V oknu *Zaposleni* izberemo ime
zaposlenega, ki ga želimo razporediti v dano izmeno. S klikom na *Mesec* izberemo mesec, v katerem želimo vpisati
razpored. S klikom na gumb *Potrdi*, se podatki v spodnjih dveh tabelah osvežijo.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/shift_shift_user.png


Prva tabela *Dodeljene ure* za izbrani mesec prikazuje v stolpcu *Pričakovano število ur na teden* za koliko ur je
delojemalec pogodbeno zaposlen in v *stolpcu Število ur v izbranem mesecu* koliko ur ima že dodeljenih na izmenah.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/shift_for_user.png

Pod tabelo je koledarski mesec razdeljen na tedne. Vsak teden je razdeljen v stolpcle, ki prikazujejo posamezni dan,
v katerem lahko razporedimo delavca. Če je vsebina stolpca za posamezni dan prazna, moramo v podmeniju *Uredi izmene*
najprej določiti *Vlogo* za izbranega zaposlenega in določiti *Urno postavko* za njegovo vlogo ter *Vlogo* vezati na
določeno *Izmeno*. Ko so vloge, postavke in izmene določene za zaposlenega, pričnemo z izbarnjem izmene za posamezne
dneve v tednu, na kateri bo zaposleni delal.


Če želimo v enem kliku zaposlenega razporediti za daljše obdobje delovnih dni, lahko v oknu *Koliko dni hkrati* vnesemo
število dni. Po pritisku gumba *Potrdi*, se v koledarju ob kliku na prvi dan dela delavec avtomatično razporedi od
tega dne za tako število dni, kot ste jih izbrali.


Več zaposlenih je možno razporejati istočasno. V oknu *Zaposleni* izberemo vse zaposlene, ki jih želimo razporediti.
Po pritisku gumba *Potrdi* se v koledarskem pogledu v prvem stolpcu pojavijo imena zaposlenih, ki jih lahko razporedimo.
Z odkljukanjem okenca *Hkratna izbira* omogočimo, da se več zaposlenim zabeleži ista vrsta razporeditve (na isti izmeni)
za več dni (opcijsko) z istim začetnim dnevom. Ta funkcionalnost poenostavi razporejanje zaposlenih, ki več dni hkrati
dela isto izmeno.


*******************
Razpored po izmenah
*******************

Podmeni *Razpored po izmenah*, nudi vpogled, kako so zaposleni razporejeni po izmenah za izbran mesec. Mesec je
razdeljen na tedne, tedni na dnevi, ti pa prikazujejo izmene, ki so izvajajo na ta dan. Pod vsako izmeno lahko vidimo
vse delavce, ki jim je bila posamezna izmena dodeljena. V obrazcu na vrhu lahko izberemo *Mesec* pogleda, v oknu
*Izmene* pa je možno filtrirati tabelo tako, da prikaže razpored samo za določeno izbrano izmeno.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/shift_by_shift.png


.. |thick| raw:: html

    <i class="icon-check"></i>

.. |pencil| raw:: html

    <i class="icon-pencil2"></i>

.. |delete| raw:: html

    <i class="icon-close"></i>

.. |object| raw:: html

    <i class="icon-file-import"></i>

.. |icon_file| raw:: html

    <i class="icon-file"></i>

.. |icon_bill| raw:: html

   <i class="icon-bill-2"></i>

.. |icon_edit| raw:: html

   <i class="icon-edit-1"></i>

.. |icon_download| raw:: html

   <i class="icon-download-2"></i>

.. |icon_check_circle| raw:: html

   <i class="icon-check-circle"></i>

.. |icon_cancel_circle| raw:: html

   <i class="icon-cancel-circle"></i>

.. |icon_close| raw:: html

   <i class="icon-close"></i>

.. |icon_trash| raw:: html

   <i class="icon-trash"></i>

.. |icon_arrow_right| raw:: html

   <i class="icon-arrow-2-right"></i>
