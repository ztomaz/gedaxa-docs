.. gedaxa documentation master file, created by
   sphinx-quickstart on Wed Apr 26 11:23:43 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Gedaxa dokumentacija
====================


Dokumentacija je organizirana v sklope kot si sledijo v navigacijskem meniju Gedaxe:

* :ref:`glavni-meni`
* :ref:`kadrovanje`
* :ref:`vodenje-projektov`
* :ref:`skladiscenje`
* :ref:`proizvodnja`
* :ref:`poslovanje`
* :ref:`nastavitve`


.. _glavni-meni:

.. toctree::
   :maxdepth: 2
   :caption: Glavni meni

   dashboard
   moj_urnik
   poslovna_analitika


.. _kadrovanje:

.. toctree::
   :maxdepth: 2
   :caption: Kadrovanje

   zaposleni
   izmene
   potni_nalogi
   obracuni_plac
   organigram


.. _vodenje-projektov:

.. toctree::
   :maxdepth: 2
   :caption: Vodenje projektov

   seznam_projektov
   casovnice_projektov
   vse_ponudbe
   vsa_narocila
   vse_dobavnice


.. _skladiscenje:

.. toctree::
   :maxdepth: 2
   :caption: Skladiščenje

   material_in_skladisca
   inventura
   ostanki


.. _proizvodnja:

.. toctree::
   :maxdepth: 2
   :caption: Proizvodnja

   sifranti
   operacije
   procesi
   stroji
   delovni_nalogi


.. _poslovanje:

.. toctree::
   :maxdepth: 2
   :caption: Poslovanje

   partnerji
   dobava
   racuni


.. _nastavitve:

.. toctree::
   :maxdepth: 2
   :caption: Nastavitve

   nastavitve


=============================
Kako mi dokumentacija pomaga?
=============================

Dokumentacija razloži kako deluje vsak del Gedaxa ERP. Lahko ga beremo na dva načina:

1. Po sklopih glavnega menija (kot si sledijo funkcionalnosti v sistemu), ali
2. Poiščemo zgolj odgovor na konkretno vprašanje.

Zgrajen je bil s podjetji iz CNC industrije, za CNC industrijo, da odgovarja na potrebe industrije. Zato je logika
sistema, da deluje kot projekt. Če vnašamo nabavo, dobavo, naročila, dodelimo delavne naloge… vse je vezano na projekte,
ki so v osrčju delovanja Gedaxa ERP.


*******************************
Kako začnem uporabljati Gedaxo?
*******************************

Vsak sistem je zgrajen iz gradnikov. Isto velja za Gedaxa ERP. Da bi sistem pravilno deloval moramo vnesti nekatere
informacije, ki jih sistem zveže med sabo:

1. Podatke o zaposlenih
2. Izmene in delovne vloge v podjetju
3. Šifrante materialov in strojev
4. Operacije, cikle in procese strojev
5. Skladišča in zaloge materiala
6. Partnerje: dobavitelje in naročnike

Brez skrbi, dokumentacija in sam sistem vas vodi skozi vnašanje potrebnih informacij.
Proces vnašanja je pa narejen na uporabniku prijazen način.


*******************
Kaj pomenijo ikone?
*******************

Vseskozi Gedaxo uporabljamo iste ikone, da lažje (tj. z manj branja in pisanja) uporabljamo Gedaxo. Vedno uporabljamo
iste ikone s sledečimi pomeni:

.. raw:: html

    <table>
        <tr>
            <th>Ikona</th>
            <th>Opis</th>
            <th>Ikona</th>
            <th>Opis</th>
        </tr>
        <tr>
            <td><i class="icon-swap-vertical"></i></td>
            <td>Filtriraj podatke v tabeli naraščajoče, padajoče</td>
            <td><i class="icon-check"></i></td>
            <td>Potrdi oziroma zaključi</td>
        </tr>
        <tr>
            <td><i class="icon-arrow-1-right"></i></td>
            <td>Poglej podrobnosti podatkov v vrstici</td>
            <td><i class="icon-close"></i></td>
            <td>Izbriši podatek</td>
        </tr>
        <tr>
            <td><i class="fa icon-calendar"></i></td>
            <td>Datum</td>
            <td><i class="icon-cancel-circle"></i></td>
            <td>Prekliči tekoči projekt</td>
        </tr>
        <tr>
            <td><i class="icon-pencil2"></i></td>
            <td>Uredi podatek / vpis</td>
            <td></td>
            <td>Preglej in uredi skupino / člane skupine</td>
        </tr>
        <tr>
            <td><i class="icon-user-male-options"></i></td>
            <td>Uredi pravice skupine</td>
            <td><i class="icon-graph"></i></td>
            <td>Statistika</td>
        </tr>
        <tr>
            <td><i class="icon-file"></i></td>
            <td>PDF dokument</td>
            <td><i class="icon-files"></i></td>
            <td>Datoteke</td>
        </tr>
        <tr>
            <td><i class="fa icon-calendar"></td>
            <td>Časovno obdobje, trajanje</td>
            <td><i class="icon-bill-2"></i></td>
            <td>Ustvari obračun potnega naloga</td>
        </tr>
        <tr>
            <td><i class="icon-clipboard-checked"></i></td>
            <td>Izdan račun</td>
            <td><i class="icon-papyrus"></i></td>
            <td>Ustvari mesečno poročilo</td>
        </tr>
    </table>

Če miško postavite nad ikono, se prikaže opis ikone in njene funkcije, da v Gedaxi čim hitreje najdete, kar iščete.

Dokumentacija za uporabo Gedaxa sistema
=======================================


.. |thick| raw:: html

    <i class="icon-check"></i>

.. |pencil| raw:: html

    <i class="icon-pencil2"></i>

.. |delete| raw:: html

    <i class="icon-close"></i>

.. |object| raw:: html

    <i class="icon-file-import"></i>

.. |icon_file| raw:: html

    <i class="icon-file"></i>

.. |icon_bill| raw:: html

   <i class="icon-bill-2"></i>

.. |icon_edit| raw:: html

   <i class="icon-edit-1"></i>

.. |icon_download| raw:: html

   <i class="icon-download-2"></i>

.. |icon_check_circle| raw:: html

   <i class="icon-check-circle"></i>

.. |icon_cancel_circle| raw:: html

   <i class="icon-cancel-circle"></i>

.. |icon_close| raw:: html

   <i class="icon-close"></i>

.. |icon_trash| raw:: html

   <i class="icon-trash"></i>

.. |icon_arrow_right| raw:: html

   <i class="icon-arrow-2-right"></i>


=========
ZAPOSLENI
=========

Ta del komunetacije se nanaša na sklop funkcionalnosti, ki jih najdete v glavnem meniju v sekciji Kadrovanje.

*****************
Seznam zaposlenih
*****************

******************
Skupine in pravice
******************

***********************
Planirana usposabljanja
***********************

********************
Odsotnost zaposlenih
********************

*********************
Prisotnost zaposlenih
*********************


=============
OBRAČUNI PLAČ
=============

================
SEZNAM PROJEKTOV
================

=============
VSE DOBAVNICE
=============

=====================
MATERIAL IN SKLADIŠČA
=====================

*Material in skladišča* nam ponuja pogled v katerem lahko urejamo material, ga preskladiščimo, pregledamo njegov
dnevnik, ter urejamo vsa naša skladišča.

********************
Kako uredim material
********************

Pred dodajanjem novega materiala, moramo poskrbeti, da se material nahaja v šifrantu (za pomoč poglejmo del
dokumentacije z naslovom *Kako dodamo nov šifrant*). Prav tako moramo imeti ustvarjeno skladišče v katerega želimo
dodati material (za pomoč poglejmo del dokumentacije z naslovom *Kako dodam novo skladišče?*).


Nov material dodamo tako, da v glavnem meniju izberemo *Material in skladišča* in nato v podmeniju možnost
*Uredi material*. Pokaže se nam tabela s seznamom vseh obstoječih materialov. Na dnu tabele izberimo možnost
*Uredi material*. S klikom na gumb *Uredi material* se nam odpre obrazec za vnos podatkov. Za *Dejanje* izberemo
eno izmed možnosti: *Dodaj na voljo* (Dodajanje materiala v skladišče), *Odstrani na voljo* (odstranjevanje materiala iz
skladišča), *Dodaj rezervacijo* (Dodajanje rezervacije na *Projekt*), *Odstrani rezervacijo* (odstranjevanje materiala
s *Projekta*, s tem tudi iz skladišča), *Spremeni rezeravcijo v na voljo* (odstranjevanje materiala s *Projekta* in
pri tem dodajanje v skladišče). S pomočjo iskalnega okna vnesemo material in skladišče.
Vnesemo še količino, ki jo želimo dodati, ter izbirno *Projekt* (v primerih, ko je ta potreben). Vnos materiala
zaključimo s klikom na gumb *Potrdi*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/material_1.png


***************************************************
Kaj pomeni stolpec Na voljo/Pričakovano/Rezervirano
***************************************************

Stolpec *Na voljo/Pričakovano/Rezervirano* nam prikazuje razmerje med materialom, ki ga lahko uporabljamo, materialom,
ki ga pričakujemo (naročilo) in materialom, ki je rezerviran za projekte. Material, ki je rezerviran je vedno
podmnožica materiala, ki je na voljo.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/material_reserved.png

****************************
Kako preskladiščimo material
****************************

Pogoj: preskladiščimo lahko samo material, ki je že v skladišču.

Če želimo material preskladiščiti iz trenutnega skladišča v skladišče, ki še ne obstaja, je potrebno novo skladišče
predhodno ustvariti (za pomoč poglejmo del dokumentacije z naslovom *Kako dodamo novo skladišče*).

Material lahko preskladiščimo na dva načina:

1. Preko grafičnega vmesnika
2. S pomočjo obrazca Uredi material

Preko grafičnega vmesnika preskladiščimo material tako, da v glavnem meniju izberemo *Material in skladišča*.
Pokaže se nam tabela s seznamom vseh materialov. Na dnu tabele s klikom na gumb *Preskladišči material* se nam odpre
obrazec za vnos podatkov.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/material_transfer.png

Izbrati moramo material iz šifranta in količino, ki predstavlja eno enoto materiala, katerega želimo prenesti. S klikom
na gumb *Potrdi* se nam odpre grafični vmesnik za preskladiščenje materiala. Material lahko z akcijo povleci in spusti
(angl. drag&drop) poljubno premikamo med skladišči. Spremembe se shranijo samodejno.

Grafični vmesnik prikaže material za preskladiščenje v kockah, ki predstavljajo količino, ki smo jo določili v
obrazcu zgoraj. Kocke dajejo vpogled nad tem, kolikšen delež celotnega materiala želimo preskladiščiti. Zelene kocke
je možno preskladiščiti, medtem ko sivih ni, saj te predstaljajo material, ki jerezerviran za določen projekt.

S pomočjo obrazcca preskladiščimo material tako, da v glavnem meniju izberemo *Urejanje materiala* in nato v.
Pokaže se nam tabela s seznamom vseh materialov. Na dnu tabele s klikom na gumb *Uredi material* se nam odpre obrazec
za vnos podatkov. Za preskladiščenje moramo izbrati dejanje *Odstrani* (pri katerem odstranjujemo material iz skladišča)
in nato dejanje *Dodaj* (v katero skladišče želimo preskladiščiti).

Omejitve: preskladiščimo lahko samo en material hkrati. Grafični mesnik za preskladiščenje materiala ne moremo
uporabljati na zaslonih na dotik.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/material_transfer_gui.png

*********************************************************
Kako preverimo kaj se je s posameznim materialom dogajalo
*********************************************************

Kdo je dodal ali odstranil material, kdo ga je rezerviral, kam se je material premikal in drugo preverimo tako, da
v glavnem meniju izberemo *Material in skladišča*. Pokaže se nam tabela s seznamom vseh materialov. S klikom na gumb
*Poglej dnevnik* se nam odpre tabela, ki vsebuje podatke o tem kdo, koliko in kateri material se je iz posameznega
skladišča premikal ter kdaj se je to zgodilo.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/material_log_1.png


.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/material_log_2.png


**************************
Kako dodamo novo skladišče
**************************

Novo skladišče dodamo tako, da v glavnem meniju izberemo *Material in skladišča* in nato v podmeniju možnost
*Uredi material*. Pokaže se nam tabela s seznamom vseh materialov. Na vrhu tabele izberimo možnost *Skladišče*.
S klikom na gumb *Dodaj skladišče* se nam odpre obrazec za vnos podatkov. *Vnesimo *Ime* (poljubno unikatno ime) in
opis. Urejanje zaključimo s klikom na gumb Potrdi.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/material_warehouse_add.png

****************************************
Kaj je skladišče pričakovanega materiala
****************************************

Skladišče pričakovanega materiala je sistemsko skladišče, ki ga ni mogoče zbrisati. V njem se nahaja material, ki smo
ga naročili pri dobavitelju vendar zanj še nismo vnesli dobavnice. Z zaključevanjem dobavnice materiala se material
preskladiši v izbrana skladišča (zaključevanje dobavnice se nahaja v sekciji *Dobavnice*).
S klikom na gumb > poleg imena skladišča se nam prikaže zaloga materiala v skladišču.

*****************************
Kaj je Nerazvrščeno skladišče
*****************************

Nerazvrščeno skladišče je sistemsko skladišče, ki ga ni mogoče zbrisati. V njem se nahajajo  produkti, ki smo jih
naredili v naših procesih in jih skladiščimo po končanem proizvodnjem procesu. S klikom na gumb > poleg imena skladišča
se prikaže zaloga materiala v skladišču.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/material_warehouse_system.png

=========
INVENTURA
=========

=======
OSTANKI
=======

========
ŠIFRANTI
========

Šifranti so edinstveni objekti v sistemu Gedaxa, ki so zapisani v obliki številk (šifer), ki predstavljajo vse
predmete, ki jih uporabljamo v naši proizvodnji, od materialov do izdelkov, od strojev do embalaže ter storitev.

Razdeljeni so ni tri dele (podmenije): *Material* (kamor spadajo izdelki, polizdelki, ter surov matrial),
*Ostalo* (orodja, stroji, kontrolna sredstva, pripomočki, pisarniška oprema, embalaža) ter *Storitve*. Za lažjo
prepoznavnost šifrantov, se ti na podlagi delitve začenjajo z različno številko (številke 1-11 predstavljene pred piko).
Številka za piko pa je samodejna naraščujoča, ki se poveča za vsak dodan nov izdelek (s tem sistem zagotavlja
unikatnost šifer). Poleg tega lahko šifrantu določimo tudi interno šifro (prvo število po piki) ter zunanjo šifro.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/codetables_1.png


***********************
Kako dodamo nov šifrant
***********************

V meniju *Šifranti* imamo vpogled do vseh naših šifrantov. Tu ji lahko tudi urejamo in dodajamo.

S pritiskom na gumb *Dodaj šifro* se odpre obrazec za vnos nove šifre. *Ime*, *Naziv 1* in *Naziv 2* so poljubna
poimenovanja za šifrant, ki ga želimo vnesti. Iz padajočega menija *Vrsta šifranta* izbiramo med možnostmi
*Izdelek*, *Polizdelek*, *Material*, *Orodja*, *Stroj*, *Kontrolna sredstva*, *Pripomočki*, *Pisarniška oprema*,
*Embalaža* in *Storitev*. Vsaka vrsta šifranta ima svojo pripadajočo številko, ki se prikaže v končni šifri kot prva
števka šifranta (številka pred piko). Posamezne vrste šifranta in njim pripadajoče številke so napisane na desni strani
pod tabelo.

Za lažjo prepoznavnost šifrantov določimo interno šifro, tj. eno števko od 0 do 9, ki bo v končni šifri dodana za piko.
Ostala števila so dodana avtomatično (s tem zagotovimo unikatnost vseh šifer). Seznam internih šifer si lahko
zabeležimo v prazno polje levo pod tabelo (in *Shranimo*), da nam je na razpolago pri prihodnjih vnosih šifrantov.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/codetables_intern_numbers.png


V naslednjem koraku vpišemo koliko kilogramov, metrov, litrov in enot predstavlja 1 kos izbrnega šifranta. Ta
zapis razmerja med enotami je kasneje uporabljen pri preračunavanju uporabe šifrantov v procesih podjetja.  Količina
kilogram je obvezno polje, ki ga moramo izpolniti. Enote morajo biti celoštevilske saj le s tem sistem zagotavlja
pravilno računanje pretvarjanja enot. Za lažje račuanje enot (v primeru da nimamo celoštevilskih faktorjev) je na voljo
tudi računalo, ki po vnosu samodejno zapiše razmerja v obrazec.

Polje *Opis* je poljubno polje, v katerega lahko zapišemo opombo na šifrant. Po pritisku gumba *Shrani* se šifrant
zabeleži v tabeli *Seznam šifrantov*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/codetables_add_1.png

Ko je šifrant vnesen, ga lahko vedno *Uredimo* |pencil| ali *Zbrišemo* |delete| s pritiskom na
ikono v zadnjem stolpcu tabele *Seznam šifrantov*. Po *Tabeli šifrantov* lahko filtriramo po imenu ali (delni) šifri
šifrantov z uporabo funkcije *Filtriraj tabelo*, ki jo najdemo desno nad tabelo. Filtriranje je uporabno za iskanje
samo določene vrste šifrantov, npr. samo strojev, z vpisom iskalnega niza “5.”.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/codetables_filter.png

=========
OPERACIJE
=========

======
STROJI
======

***************
PREGLED STROJEV
***************

*****
CIKLI
*****

***********
VZDRŽEVANJE
***********

==============
DELOVNI NALOGI
==============

***********************
SEZNAM DELOVNIH NALOGOV
***********************

Kako ustvarim nov delovni nalog?
********************************
**Pogoj**: Da bi lahko ustvarili delovni nalog, moramo imeti vpisano naročilo naročnika. Če nimamo, glej *Kako ustvarim novo naročilo*?

Ko želimo ustvariti delovni nalog, odpremo zavihek v meniju Seznam delovnih nalogov (Glavni meni > Poslovanje > Delovni nalogi > Seznam delovnih nalogov) in kliknemo na gumb Dodaj delovni nalog. Odpre se forma za vnos, kjer izberemo naročilo, na katero se delovni nalog nanaša in potrdimo izbiro.

***************
MOJE ZADOLŽITVE
***************

*******************
CELOVIT POGLED DELA
*******************

======
DOBAVA
======

V dobavi najdemo vse kar je povezano z naročili dobave in dobavnicami. Vsa naročila in dobavnice so vezane na
dobavitelja, ter jih lahko začnemo ustvarjati šele ko imamo te definirane (gledamo sekcijo *Kako ustvarimo partnerja*)
Ko je naročilo ustvarjeno, se postavke na naročilu dodajo v skladišče pričakovanega materiala, ob zapiranju dobavnice pa
postavke in njihovo količino razporedimo v posamezna skladišča.

**************
SEZNAM NAROČIL
**************

V seznamu naročil vidimo vsa naročila, ki so bila ustvarjena v sistemu. Naročila imajo različna stanja (*Odprto*,
*Potrjeno* in *Preklicano*). Ko ustvarimo naročilo dobave ta dobi stanje *Odprto* in jo lahko urejamo vse dokler je ne
*Potridimo* oz. *Prekličemo*. Ko naročilo potrdimo se postavke in njihova količina zabeležita v *Skladišče pričakovanega
materiala* (gledamo sekcijo *Kaj je skladišče pričakovanega materiala*), ter ostane tam dokler ne zaključimo dobavnice,
ki se nanaša na to *Naročilo*

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/narocila/procurement.png


Kako ustvarimo naročilo dobave
++++++++++++++++++++++++++++++

V *Seznamu naročil*, kliknemo na gumb *Dodaj naročilnico*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/narocila/procurement_add_1.png

Odpre se novo okno *Ustvari naročilnico*, kjer izpolnimo
manjkajoče podatke. Če dobavitelja ni na seznamu, ga moramo vnesti kot *Partnerja*, ali pa obstoječega partnerja
označiti kot dobavitelja (gledamo *Kako dodam novega partnerja, dobavitelja in/ali naročnika*).
Po kliku *Shrani*, se naročilo shrani, nato pa lahko začnemo dodajati postavke.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/narocila/procurement_add_2.png

Kako dodamo postavke in storitve na naročilo dobave
+++++++++++++++++++++++++++++++++++++++++++++++++++

V Seznamu naročil (*Dobava* > *Seznam naročil*), kliknemo na ikono za urejanje |pencil| v stolpcu *Ukazi*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/narocila/procurement_add_items_1.png

Odpre se okno *Posodobitve naročilnice*, kjer izberemo zavihek *Postavke naročila* (poleg zavihka *Glavno*), nato
pa pritisnemo gumb *Dodaj postavko*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/narocila/procurement_add_items_2.png

Odpre se obrazec za vnos podatkov, kjer iz padajočega menija izberemo postavko (če je ni, jo dodamo med šifrante,
glej *Kako dodamo nov šifrant*), količino postavke v izbrani merski enoti, cena na enoto in rok dostave postavke.

Pravilno shranjena postavka se prikaže v tabeli *Postavke naročila* na isti strani. Če želimo, jo lahko izbrišemo
ali uredimo s klikom na ikono v okolju *Ukazi*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/narocila/procurement_edit_items.png

Kako uredimo, izbrišemo, potrdimo, prekličemo naročilo ali nanj dodamo datoteke
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

V *Seznamu naročil* (*Dobava* > *Seznam naročil*) v stolpcu *Ukazi* najdemo v kolikor je naročilo odprto 5 ikon,
durgače 3 ikone, s katerimi lahko naročilo dobave uredimo, izpišemo, nanj dodamo datoteke, potrdimo ali ga prekličemo
ter izbrišemo. Urejanje, potrjevanje, preklicevanje naročila je možno v kolikor je njegovo stanje odprto.
Naročilo pa lahko izpišemo po tem ko smo ga potrdili.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/narocila/procurement_confirm.png

Zakaj ne morem dodati postavke na naročilo?
+++++++++++++++++++++++++++++++++++++++++++

Najverjetneje, ker je bilo naročilo potrjeno ali preklicano. V tem primeru moramo narediti novo naročilo dobave,
kamor dodajamo postavke.

*********
DOBAVNICE
*********

Dobavnice so vezane na naročila dobave. Ko je naročilo potrjeno se postavke in njihova količina zabeležijo v *Skladišču
pričakovanega materiala* (gledamo sekcijo *Kaj je skladišče pričakovanega materiala*), tam pa ostanejo dokler z
dobavnicami ne zaključimo celotne količine pričakovanega materiala (eno naročilo lahko zaključujemo z večimi
dobavnicami).

Kako ustvarim dobavnico
+++++++++++++++++++++++

V pogledu *Dobavnice* (*Dobava* > *Dobavnice*) kliknemo na gumb *Ustvari dobavnico*, ki odpre obrazec za vnos podatkov.
Šifro izberemo poljubno, dobavitelja pa izberemo iz padajočega menija. Če ne najdemo dobavitelja med možnostmi, moramo
dobavitelja vnesti (gledamo sekcijo *Kako dodam novega partnerja, dobavitelja in/ali naročnika*).

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_delivery_notes.png

Ko imamo dobavnico ustvarjeno, lahko začnemo z zaključevanjem dobavljenih artiklov na posamezna naročila, ki so
bila poslana dobavitelju, katerega smo izbrali v prejšnem koraku. V pogledu *Seznam
dobavnic* v polju *Ukazi* kliknemo gumb za urejanje |pencil| (urejamo lahko vse dobavnice, ki še niso zaključene).
Ta nas pelje v novo okno, kjer so vidna vsa odprta naročila (naročila, ki še nimajo zaključene vseh količin postavk).

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_delivery_notes_edit.png

Dobavnica je razdeljena na vsa nezaključena naročila (naročila katere količine postavk še ni bila v celoti zaprta), ki
so bila poslana dobaviteljem. Za posamezno naročilo lahko zaključujemo vse postavke, vendar samo do količine, ki smo jo
definirali na samem naročilu. V kolikor je bil del postavke naročila zaključen že z drugo dobavnico (naročilo lahko
zaključujemo z večimi dobavnicami) nam sistem dovoli zaključiti le še preostanek količine, ki jo moramo še zaključiti.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_delivery_notes_orders.png

Za posamezno postavko sistem prikazuje količino, ki smo jo definirali na naročilnico, količino koliko je bilo že zaprto
ter količino koliko moramo še zapreti. S pritiskom na gumb > se nam odpre obrazec za zaključevanje postavk naročila.
Izbrati moramo skladišče v katerega bomo posamezne postavke prenesli, sistem pa ob zaključevanju dobavnice samodejno
popravi zalogo teh artiklov.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_delivery_notes_to_close.png

Na posamezno dobavnico lahko dodajamo storitve, ki smo jih definirali v šifrantih (gledamo sekcijo *Kako dodajamo
šifrant*). Poleg tega lahko dobavnico poljubno shranjujemo, ter jo urejamo dokler je ne zaključimo.

Ko dobavnico v celoti izpolnemo jo lahko zaključimo s pritiskom na gumb *Zaključi*. Sistem samodejno doda artikle v
izbrana skladišča.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_delivery_notes_finish.png

Ko je dobavnica zaključena jo lahko v pogledu *Seznam dobavnic* tudi izpišemo.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_delivery_notes_print.png

Kako dodamo datoteko na dobavnico
++++++++++++++++++++++++++++++++

V podmeniju *Vse dobavnice* (Dobava > Dobavnice), kliknemo na ikono za datoteke v vrstici dobavnice, na katero
želimo dodati datoteke. Odpre se novo okno Datoteke dobavnice.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_delivery_notes_add_file_1.png


Datoteko dodamo v dveh korakih. Najprej kliknemo gumb *Dodaj datoteko*, nato pa v obrazcu za vnos naložimo dadoteko
iz računalnika, izberemo opis datoteke in stisnemo gumb *Shrani*.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/procurement_delivery_notes_add_file_2.png

