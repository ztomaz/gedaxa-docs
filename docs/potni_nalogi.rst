============
POTNI NALOGI
============

**************************
Kako ustvarim potni nalog?
**************************

V glavnem meniju izberemo podmeni Potni nalogi, ki se nahaja v poglavju Kadrovanje.
Nato kliknemo na gumb Dodaj potni nalog, ki na desnem robu spletne strani odpre obrazec za vnos podatkov.
Vnosna polja so skladna z zakonskimi zahtevami, zato so polja Prosilec, Izdajatelj, Relacija, Kratek opis, Datum odhoda, Datum prihoda, Prevozno sredstvo in Kilometrina obvezna.
Obrazec omogoča, da zabeležimo, če smo zaposlenemu vnaprej dali avansno plačilo za službeno pot.
S klikom na gumb Potrdi se potni nalog zabeleži v seznamu z naslovom Seznam potnih nalogov.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/potni_nalogi/list.png

Potnemu nalogu lahko pripnemo dodatne dokumente preko obrazca na zavihku Datoteke.
S klikom na gumb Dodaj datoteko se nam na desnem robu strani odpre obrazec za izbiro datoteke.
Ko smo z izbiro zadovoljni, kliknemo gumb Dodaj datoteko na vnosni formi.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/potni_nalogi/datoteke.png

**********************************
Kako prenesemo PDF potnega naloga?
**********************************
V glavnem meniju izberemo pogled Potni nalog in poiščemo v Seznamu potnih nalogov željen vnos.
V zadnjem stolpcu tabele kliknemo na ikono za prenos potnega naloga |icon_file|, ki ga lahko natisnemo in posredujemo v podpis za odobritev.


**************************
Kako odobrimo potni nalog?
**************************

Potni nalog odobrimo s podpisom natisnene kopije.
Za razlago glej dokumentacijo z naslovom "Kako prenesemo PDF potnega naloga?".

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/potni_nalogi/prenos.png

************************************
Kako naredim obračun potnega naloga?
************************************

V glavnem meniju izberemo podmeni Potni nalog in v Seznamu potnih nalogov izberemo dokument, za katerega želimo narediti obračun.
S klikom na ikono |icon_bill| se odpre nova vnosna forma Ustvari obračun potnega naloga.
Forma je predizpolnjena z informacijami, ki smo jih vnesli, ko smo ustvarili potni nalog.
Informacije lahko spremenimo po želji ali dodamo nove, npr. število in vrednost dnevnic, stanje števca prevoznega sredstva na začetku in koncu potovanja, ostale stroške.
V oknu Ostali stroški prosto vpisujemo podatke in program sam izračuna njihov seštevek v oknu Cena drugih stroškov (predizpolnjena vrednost je seštevek vseh števil v prejšnjem polju).
Na obračun potnega naloga lahko dodamo razne datoteke kot so vinjete, slike računov, itd. v zavihku Datoteke.
Ko smo zadovoljni z vsemi informacijami, kliknemo gumb Potrdi na dnu.
Obračun se shrani v Seznamu potnih nalogov.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/potni_nalogi/obracun.png


*********************************************
Kako uredim obračun potnega naloga?
*********************************************
V glavnem meniju izberemo Potni nalogi in v Seznamu potnih nalogov najdemo vrstico potnega naloga, ki ga želimo urediti.
S klikom na ikono |icon_edit| v stolpcu Ukazi, se odpre forma za posodobitev podatkov, ki je enaka tisti, s katero obračun ustvarimo.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/potni_nalogi/obracun_posodobi.png

*****************************************
Kako izvozim PDF obračuna potnega naloga?
*****************************************

V glavnem meniju izberemo Potni nalogi in v Seznamu potnih nalogov najdemo vrstico potnega naloga, ki ga želimo izvoziti.
S klikom na ikono |icon_download| v stolpcu Ukazi izvozimo PDF obračuna potnega naloga.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/potni_nalogi/obracun_prenesi.png

*****************************************
Kako je določena številka potnega naloga?
*****************************************

Številka potnega naloga je unikatno število, pod katero je v sistem shranjen vsak potni nalog.
Sestavljajo jo dve števili: letnica izdaje potnega naloga in zaporedna številka potnega naloga, ki ju loči vezaj.
Tako je 2016-42 številka dvainštiridesetega potnega naloga narejenega leta 2016.

.. |thick| raw:: html

    <i class="icon-check"></i>

.. |pencil| raw:: html

    <i class="icon-pencil2"></i>

.. |delete| raw:: html

    <i class="icon-close"></i>

.. |object| raw:: html

    <i class="icon-file-import"></i>

.. |icon_file| raw:: html

    <i class="icon-file"></i>

.. |icon_bill| raw:: html

   <i class="icon-bill-2"></i>

.. |icon_edit| raw:: html

   <i class="icon-edit-1"></i>

.. |icon_download| raw:: html

   <i class="icon-download-2"></i>

.. |icon_check_circle| raw:: html

   <i class="icon-check-circle"></i>

.. |icon_cancel_circle| raw:: html

   <i class="icon-cancel-circle"></i>

.. |icon_close| raw:: html

   <i class="icon-close"></i>

.. |icon_trash| raw:: html

   <i class="icon-trash"></i>

.. |icon_arrow_right| raw:: html

   <i class="icon-arrow-2-right"></i>
