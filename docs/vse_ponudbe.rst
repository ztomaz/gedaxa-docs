===========
VSE PONUDBE
===========

************************************************
Kako ustvarim ponudbo za naročnika s postavkami?
************************************************

Ponudbo lahko ustvarimo na tri načine:


1. Znotraj **podrobnosti projekta** (*Vodenje projektov > Seznam projektov > klik na Ime projekta >*
*Ponudbe projekta > klik na gumb + Nova ponudba*). Za več informacij glej *Kako pogledam podrobnosti projekta?* in
*Kako ustvarim ponudbo znotraj Podrobnosti projekta?*

2. Na časovnici projekta (*Vodenje projektov > Časovnice projektov*). Glej dokumentacijo *Časovnica naročil* za več informacij.

3. Znotraj **vseh ponudb** (*Vodenje projektov > Vse ponudbe > Dodaj ponudbo*). **V tem sklopu predstavimo ta postopek**.

V glavnem meniju *Vodenje projektov* izberemo podmeni *Vse ponudbe*. Kliknemo na gumb *Dodaj ponudbo*
in vpišemo vse informacije, ki so relevantne za ponudbo.
Sistem avtomatično generira unikatno šifro za ponudbo, pod katero je shranjena v sistemu.

**Pogoji**: Za ustvarjanje ponudbe moramo imeti vpisane naročnike (če ga še nimamo, glej v dokumentaciji
*Kako dodam novega partnerja, dobavitelja in/ali naročnika?*), projekte (če projekt, za katerega ustvarjamo ponudbe,
še nimamo, glej dokumentacijo *Kako ustvarim nov projekt?*) in postavke, ki želimo dodati na projekt kot šifrante
(če postavk še nimamo pod šifranti, glej *Kako ustvarim šifrant?*).

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/ponudba_nova.png

Ko stisnemo *Shrani* se nam odpre nov pogled *Posodobitve ponudbe* z dvemi zavihki;
zavihek *Glavno* (vsebuje informacije, ki smo jih ravnokar vpisali) in zavihek *Postavke ponudbe*. S klikom na gumb
*Dodaj postavko* se odpre forma za dodajanje postavk. S klikom na gumb
*Dodaj storitev* se odpre forma za dodajanje storitev. Postavko izberemo iz padajočega menija, ki črpa informacije iz
baze šifrantov. Če postavke ni na voljo, jo moramo dodati kot nov šifrant, glej *Kako dodam šifrant?*.
V formi določimo Količino postavke, Enoto (kg, m, l, …) koliko postavke dodamo na ponudbo in
Ceno postavke na enoto. Okna *Količina na obdobje* in *Obdobje* se nanašajo na to, koliko postavke lahko proizvedemo v
določenem obdobju.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/ponudba_postavke.png

Ko je ponudba narejena se shrani v *Seznam ponudb* (*Vodenje projektov > Vse ponudbe*) kot odprta ponudba
(status v zadnjem stolpcu).

****************************************************
Kako uredim, potrdim, prekličem ali zbrišem ponudbo?
****************************************************

Ko je ponudba ustvarjena (glej Kako ustvarim ponudbo?) se shrani v tabelo Seznam ponudb (*Vodenje projektov > Vse ponudbe*)
kot odprta ponudba. Vsaka ponudba je v svoji vrstici tabele. Če postavimo miško nad ikono v predzadnjem stolpcu *Ukazi*,
se pojavijo različne možnosti, kaj lahko naredimo s ponudbo. Ponudbo lahko uredimo |pencil| , izvozimo PDF ponudbe
(da pošljemo naročniku) , dodamo datoteke na ponudbo |icon_file|, ponudbo potrdimo |icon_check_circle|,
prekličemo |icon_cancel_circle| ali izbrišemo |delete|.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/ponudba_seznam.png

********************************************
Kako uredim ali zbrišem postavke iz ponudbe?
********************************************

Postavke lahko urejamo ali brišemo samo na odprtih ponudbah.
V Seznamu ponudb (*Vodenje projektov > Vse ponudbe*) kliknemo na ikono za urejanje ponudbe |pencil|.
Odpre se okno *Posodobitev projekta* z dvema zavihkoma, *Glavno* in *Postavke ponudbe*. Izberemo slednjega in v
zadnjem stolpcu kliknemo ikono za urejanje |pencil|, v zadnjem stolpcu prav tako najdemo ikono za brisanje postavke |delete|.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/ponudba_edit.png

*************************
Kako izvozim pdf ponudbe?
*************************

Ustvarjene ponudbe so shranjene v tabeli *Seznam ponudb* (*Vodenje projektov > Vse ponudbe*). Vsaka ponudba je v svoji
vrstici tabele. Če postavimo miško nad ikono v predzadnjem stolpcu *Ukazi*, se pojavijo različne možnosti,
kaj lahko naredimo s ponudbo. Klik na ikono |icon_file| omogoča izvoz ponudbe kot PDF.

.. |thick| raw:: html

    <i class="icon-check"></i>

.. |pencil| raw:: html

    <i class="icon-pencil2"></i>

.. |delete| raw:: html

    <i class="icon-close"></i>

.. |object| raw:: html

    <i class="icon-file-import"></i>

.. |icon_file| raw:: html

    <i class="icon-file"></i>

.. |icon_bill| raw:: html

   <i class="icon-bill-2"></i>

.. |icon_edit| raw:: html

   <i class="icon-edit-1"></i>

.. |icon_download| raw:: html

   <i class="icon-download-2"></i>

.. |icon_check_circle| raw:: html

   <i class="icon-check-circle"></i>

.. |icon_cancel_circle| raw:: html

   <i class="icon-cancel-circle"></i>

.. |icon_close| raw:: html

   <i class="icon-close"></i>

.. |icon_trash| raw:: html

   <i class="icon-trash"></i>

.. |icon_arrow_right| raw:: html

   <i class="icon-arrow-2-right"></i>
