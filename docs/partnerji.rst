=========
PARTNERJI
=========

***************
UREDI PARTNERJA
***************

Kako dodamo novega partnerja, dobavitelja in/ali naročnika?
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Dobavitelje, naročnike in nasploh poslovne partnerje dodajamo znotraj opcije Uredi partnerja v podmeniju Partnerji.
Imamo dve možnosti dodajanja partnerjev: ali vnesemo vse podatke ročno v formo, ki se odpre z izbiro Dodaj partnerja,
ali pa izberemo Dodaj partnerja iz javnega registra (PRS), kjer forma sama poišče vse podatke o poslovnem partnerju.
V obeh primerih za poslovenga partnerja izberemo ali je dobavitelj, naročnik, ali oboje.

**Pozor**: Dodajanje partnerja iz javnega registra (PRS) izbere najbolj sodobne informacije o poslovnem subjektu zgolj v
času dodajanja subjekta znotraj partnerjev. Sistem ne beleži nadaljnih sprememb o podjetjih v poslovnem registru.
Za posodabljanje informacij o poslovnih partnerjih ste odgovorni sami.
Ko smo dodali vse partnerje se ti prikažejo v tabeli. Partnerje lahko nato urejamo (opcija Uredi) ali odstranimo
(opcija Izbriši) z izbiro ustrezne ikone v stolpcu Ukazi.


*******************
CENE IN ČASI DOBAVE
*******************

Kako dodamo ceno in čas dobave za izbrano postavko?
+++++++++++++++++++++++++++++++++++++++++++++++++++

V pogledu Cene in časi dobave znotraj podmenija Partnerji izberemo gumb Dodaj dobavni čas. V novo odprtem formularju
izberemo Dobavitelja iz baze podatkov. Če dobavitelja še nismo vnesli, lahko to storimo v podmeniju Partnerji
(glej Kako dodam novega partnerja, dobavitelja in/ali naročnika?). Iz padajočega menija izberemo Material,
ki ga imamo zabeleženega med šifranti (če materiala še nismo vpisali, to storimo najprej; glej Kako dodamo nov
šifrant?). Vpišemo Čas dobave v dnevih in Ceno postavke na enoto, kjer izberemo Enoto, za izbranega dobavitelja.

.. image:: https://gitlab.com/projekti/gedaxa-docs/raw/master/imgs/dobavni-casi.png

Čas dobave in cena postavk je prikazana v tabeli, kjer vsaka vrstica predstavlja postavko določenega dobavitelja.
Podatke lahko urejamo (opcija Uredi) ali brišemo (opcija Izbriši) s pritiskom ustrezne ikone v stolpcu Ukazi.
